# Mini Projects

This repository is for documenting any small projects of mine (both finished and not). The projects are mainly done with the goal of learning or trying something new, e.g. a language or library/package.