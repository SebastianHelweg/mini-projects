board = [["[ ]", "[ ]", "[ ]"],["[ ]", "[ ]", "[ ]"],["[ ]", "[ ]", "[ ]"]]

def main():
    player1, player2 = 0, 0

    while 1:
        for n in board:
            print(n)
        
        if player1 == player2:
            turnInput = turn(1)
            board[int(turnInput[0])][int(turnInput[1])] = "[X]"
            player1 += 1
        elif player2 <= player1:
            turnInput = turn(2)
            board[int(turnInput[0])][int(turnInput[1])] = "[O]"
            player2 += 1

        if turnInput == -1:
            break
        
def turn(player):
    output = "Player {}'s turn:"
    turnInput = input("Player {}'s turn:".format(player))
    
    return turnInput.split()

#def check():


if __name__ == "__main__":
    main()